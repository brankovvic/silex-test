<?php

use Silex\WebTestCase;

class controllersTest extends WebTestCase
{
    public function testNotValidResponseFromApi()
    {
        $client = $this->createClient();
        $client->request('GET', '/location/info');

        $this->assertTrue($client->getResponse()->isOk());

        $this->assertEquals($client->getResponse()->getContent(), '{"error":"Ip address is invalid!"}');
    }

    public function testValidResponseFromApi()
    {
        $client = $this->createClient();

        $client->request('GET', '/location/info?ip_address=208.67.222.222');

        $this->assertTrue($client->getResponse()->isOk());

        $this->assertEquals($client->getResponse()->getContent(), '{"city":"Howard Beach","country":"US"}');
    }

    public function createApplication()
    {
        $app = require __DIR__.'/../src/app.php';
        require __DIR__.'/../config/dev.php';
        require __DIR__.'/../src/controllers.php';
        $app['session.test'] = true;

        return $this->app = $app;
    }
}
