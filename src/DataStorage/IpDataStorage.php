<?php

namespace DataStorage;

class IpDataStorage
{
    private $ipAddress;

    /**
     * @param null|string $ipAddress
     */
    public function setIpAddress(?string $ipAddress): void {
        $this->ipAddress = $ipAddress;
    }

    /**
     * @return null|string
     */
    public function ipAddress(): ?string {
        return $this->ipAddress;
    }
}
