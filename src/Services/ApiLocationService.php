<?php

namespace Services;

use Services\Interfaces\ApiLocationServiceInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ApiLocationService implements ApiLocationServiceInterface
{
    static $API_URL = "https://ipinfo.io/";

    /**
     * It returns object with user ip information
     * If ip is default value it returns the info for the current ip address
     *
     * @param string $ip
     *
     * @return array| null
     */
    public function queryApiForIpInformation(string $ip = ""): ?array
    {
        $response = @file_get_contents(self::$API_URL . $ip);

        return ($response) ? json_decode($response, true) : null;
    }
}
