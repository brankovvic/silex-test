<?php

namespace Services\Interfaces;

interface ApiLocationServiceInterface
{
    /**
     * It returns object with user ip information
     * If ip is default value it returns the info for the current ip address
     *
     * @param string $ip
     *
     * @return array| null
     */
    public function queryApiForIpInformation(string $ip = ""): ?array;
}
