<?php

namespace Services\Interfaces;

interface LocationServiceInterface
{
    /**
     * Returns country and city for user ip address
     * @param string $ip
     *
     * @return array|null
     */
    public function userLocationInfo(string $ip): ?array;

    /**
     * Check if ip record exists in database and returns country and city for ip address
     * @param string $ip
     *
     * @return array|null
     */
    public function ipRecordExistsInDatabase(string $ip): ?array;

    /**
     * Saves new ip record information in database
     * @param string $ip
     * @param string $city
     * @param string $country
     *
     * @return bool
     */
    public function saveNewIpLocationInfoInDatabase(string $ip, string $city, string $country): bool;
}
