<?php

namespace Services;

use Models\LocationInfo;
use Services\Interfaces\ApiLocationServiceInterface;
use Services\Interfaces\LocationServiceInterface;

class LocationService implements LocationServiceInterface
{
    /**
     * @var ApiLocationServiceInterface
     */
    private $apiLocationService;
    /**
     * @var LocationInfo
     */
    private $locationInfo;

    public function __construct(
        ApiLocationServiceInterface $apiLocationService,
        LocationInfo $locationInfo
    ) {
        $this->apiLocationService = $apiLocationService;
        $this->locationInfo = $locationInfo;
    }

    /**
     * Returns country and city for user ip address
     *
     * @param string $ip
     *
     * @return array|null
     */
    public function userLocationInfo(string $ip = ""): ?array
    {

        $locationInfo = $this->apiLocationService->queryApiForIpInformation($ip);

        return (is_null($locationInfo) || !empty($locationInfo['bogon'])) ? null
            : ['city' => $locationInfo['city'], 'country' => $locationInfo['country']];
    }

    /**
     * Check if ip record exists in database and returns country and city for ip address
     *
     * @param string $ip
     *
     * @return array|null
     */
    public function ipRecordExistsInDatabase(string $ip): ?array
    {
        $locationInfo = $this->locationInfo->find('first', ['conditions' => ['ip_address=?', $ip]]);

        return (is_null($locationInfo))
            ? $locationInfo
            : [
                'city' => $locationInfo->city,
                'country' => $locationInfo->country,
            ];
    }

    /**
     * Saves new ip record information in database
     *
     * @param string $ip
     * @param string $city
     * @param string $country
     *
     * @return bool
     */
    public function saveNewIpLocationInfoInDatabase(string $ip, string $city, string $country): bool
    {
        return ($this->locationInfo->create(
            [
                'ip_address' => $ip,
                'city' => $city,
                'country' => $country,
            ]
        ) instanceof LocationInfo ? true : false);
    }
}
