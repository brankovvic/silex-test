<?php

use Controllers\LocationController;
use Models\LocationInfo;
use Services\ApiLocationService;
use Services\LocationService;
use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Silex\Provider\FormServiceProvider;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\HttpFoundation\Request;

$app = new Application();
$app->register(new ServiceControllerServiceProvider());
$app->register(new AssetServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(new FormServiceProvider());

$dotenv = new Dotenv();
$dotenv->load(__DIR__ . '/../.env');

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/Views',
));

ActiveRecord\Config::initialize(
    function ($cfg) {
        $cfg->set_model_directory(__DIR__ . '/../src/Models');
        $cfg->set_connections(
            [
                'development' => "mysql://"
                                 . getenv('DB_USERNAME')
                                 . ":"
                                 . getenv('DB_PASSWORD')
                                 . "@"
                                 . getenv('DB_HOST')
                                 . "/" . getenv('DB_NAME'),
            ]
        );
    }
);

$app['location.model'] = $app->factory(function() {
    return new LocationInfo();
});

$app['apiLocation.service'] = function () {
    return new ApiLocationService();
};

$app['location.service'] = $app->factory(
    function () use ($app) {
        return new LocationService($app['apiLocation.service'], $app['location.model']);
    }
);

$app['location.controller'] = function () use ($app) {
    return new LocationController($app['location.service']);
};

$app->get('/', function (Request $request) use ($app) {
    return $app['twig']->render('index.html.twig', array('ipAddress'=>$request->getClientIp()));
});

$app->get('/location/info', 'location.controller:locationIndex')->bind('locationInfo');

return $app;
