<?

namespace Controllers;

use Services\Interfaces\LocationServiceInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class LocationController
{
    /**
     * @var LocationServiceInterface
     */
    private $locationService;

    public function __construct(LocationServiceInterface $locationService)
    {
        $this->locationService = $locationService;
    }

    public function locationIndex(Request $request): JsonResponse
    {

        $ipValidAddress = filter_var($request->get('ip_address'), FILTER_VALIDATE_IP);

        if ($ipValidAddress === false) {
            return JsonResponse::create(['error' => 'Ip address is invalid!']);
        }

        $ipAddressInfo = $this->locationService->ipRecordExistsInDatabase($ipValidAddress);

        if (is_null($ipAddressInfo)) {
            $ipAddressInfo = $this->locationService->userLocationInfo($ipValidAddress);

            if (! is_null($ipAddressInfo)) {
                $this->locationService->saveNewIpLocationInfoInDatabase(
                    $ipValidAddress,
                    $ipAddressInfo['city'],
                    $ipAddressInfo['country']
                );
            }
        }

        return JsonResponse::create($ipAddressInfo);
    }
}
