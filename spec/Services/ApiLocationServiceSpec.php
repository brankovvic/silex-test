<?php

namespace spec\Services;

use Services\ApiLocationService;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ApiLocationServiceSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(ApiLocationService::class);
    }

    function it_should_return_null_for_bad_request()
    {
        $this->queryApiForIpInformation(1)->shouldBe(null);
    }

    function it_should_return_empty_object()
    {
        $this->queryApiForIpInformation("208.67.222.222")
            ->shouldHaveKey('city');
    }
}
