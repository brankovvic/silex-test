<?php

namespace spec\Services;

use Models\LocationInfo;
use Services\ApiLocationService;
use Services\LocationService;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class LocationServiceSpec extends ObjectBehavior
{
    function let(ApiLocationService $apiLocationService, LocationInfo $locationInfo)
    {
        $this->beConstructedWith($apiLocationService, $locationInfo);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(LocationService::class);
    }

    function it_should_return_null_for_non_existing_ip_address($apiLocationService)
    {
        $apiLocationService->queryApiForIpInformation(1)->willReturn(null);

        $this->userLocationInfo(1)->shouldReturn(null);
    }

    function it_should_return_country_and_city($apiLocationService)
    {
        $data = ['city'=>'Belgrade', 'country'=>'Serbia'];

        $apiLocationService->queryApiForIpInformation("208.67.222.222")->willReturn($data);

        $this->userLocationInfo("208.67.222.222")->shouldReturn($data);
    }

    /*
     * This cannot be tested because the active record somewhere are calling static method
     * it should be wrapped in some service or called using facade but it needs to be researched
     *
    function it_should_return_not_found_in_database($locationInfo)
    {
        $locationInfo->find('first', ['conditions'=> ['ip_address=?', "8.8.8.8"]])->shouldBeCalled();


        $this->ipRecordExistsInDatabase("8.8.8.8")->shouldReturn(null);
    }*/
}
