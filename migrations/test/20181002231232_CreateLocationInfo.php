<?php

class CreateLocationInfo extends Ruckusing_Migration_Base
{
    public function up()
    {
        $locationInfo = $this->create_table("location_info");
        $locationInfo->column("ip_address", "string", ['null' => false]);
        $locationInfo->column("city", "string");
        $locationInfo->column("country", "string");
        $locationInfo->finish();
    }//up()

    public function down()
    {
        $this->drop_table('location_info');
    }//down()
}
