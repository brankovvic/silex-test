Test Application For Fetching User GeoLocation
==============

Welcome to my test application for fetching geolocation by ip.

Application is very simple. It uses client ip address to fetch geolocation, free ipinfo.io is used to
fetch client geolocation.

Application is using symfony silex micro framework(https://silex.symfony.com)
with ActiveRecord(https://packagist.org/packages/php-activerecord/php-activerecord) for database management.

Composer is used Dependency Manager!


Setup Application
----------------------------

After cloning the project use composer to install dependencies

.. code-block:: console

    $ composer install

After composer finishes installing all the dependencies, edit file

.. code-block:: console

    $.env

and update database parameters

Run migrations to migrate tables:

.. code-block:: console

    $php vendor/bin/ruckus.php db:migrate


Running Tests
-----------------------------

If database is setup and all the dependecies are installed tests needs to be run to insure
that application is working as intended

Run phpspec tests:

.. code-block:: console

    $php vendor/bin/phpspec run

Run phpunit tests:

.. code-block:: console

    $php vendor/bin/phpunit tests

If all tests are successful you're now ready to use application.


Running Application
-----------------------------
To see a real-live application page in action, start the PHP built-in web server with
command:

.. code-block:: console

    $ COMPOSER_PROCESS_TIMEOUT=0 composer run

Then, browse to http://localhost:8888/index_dev.php

Enjoy!
